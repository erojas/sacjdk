<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    	DB::table('roles')->insert([
      ['id' => 1, 'codigo' => "admin", 'nombre' => 'Administrador', 'estado' => true,'created_at' => date("Y-m-d H:i:s.u"), "updated_at" => date(date("Y-m-d H:i:s.u"))]]);
    	DB::table('users')->insert([
      ['id' => 1, 'nombre' => 'admin','apellido'=>'rojas', 'dui'=>'124','estado'=>true, 'email' => 'efrain@gmail.com', 'password' => bcrypt('123456'),'rol_id'=>1, 'created_at' => date("Y-m-d H:i:s.u"), "updated_at" => date(date("Y-m-d H:i:s.u")),]
    ]);
    }
}
